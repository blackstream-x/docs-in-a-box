# Build environment

The `Dockerfile` and `mkdocs.yml` files are parameterized via environment variables
set in the pipeline configuration (`.gitlab-ci.yml`).

The following parameters are supported:

| Variable name         | Used in                | Description                                                      |
| :-------------------- | :--------------------- | :--------------------------------------------------------------- |
| `DESCRIPTION`         | Dockerfile, mkdocs     | Description for site and container metadata                      |
| `DOCS_AUTHOR`         | mkdocs                 | Author(s) of the documentation                                   |
| `DOCS_COPYRIGHT`      | mkdocs                 | Copyright notice at the page bottom                              |
| `DOCS_DEPLOYMENT_URL` | mkdocs                 | URL of the target site, default: <http://localhost:8080/>        |
| `DOCS_LOCALE`         | mkdocs                 | ISO 639-1 code of the language used in the docs                  |
| `DOCS_TITLE`          | mkdocs                 | Site title                                                       |
| `EXPOSED_PORT`        | Dockerfile, nginx.conf | Port exposed by the docker container, default: `8080`            |
| `IMAGE_MAINTAINERS`   | Dockerfile             | Maintainers of the container image                               |
| `IMAGE_NAME`          | Dockerfile             | Name of the built container image                                |
| `NGINX_BASE_IMAGE`    | Dockerfile             | The nginx base image (see <https://hub.docker.com/_/nginx/tags>) |
| `NGINX_SERVER_NAME`   | nginx.conf             | The server name, default: `localhost`                            |

