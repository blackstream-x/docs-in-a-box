ARG NGINX_BASE_IMAGE="nginx:mainline-alpine-slim"
FROM $NGINX_BASE_IMAGE

ARG DOCS_HTML_DIRECTORY="html"

COPY webserver/nginx.conf /etc/nginx/
COPY $DOCS_HTML_DIRECTORY /usr/share/nginx/html

ARG DESCRIPTION="Documentation website capsuled in a Container, on top of nginx"
ARG EXPOSED_PORT="8080"
ARG IMAGE_MAINTAINERS="Documentation maintainers"
ARG IMAGE_TAG="unspecified"

LABEL description="$DESCRIPTION"
LABEL maintainer="$IMAGE_MAINTAINERS"
LABEL org.opencontainers.image.authors="$IMAGE_MAINTAINERS"
LABEL version="$IMAGE_TAG"

USER nobody

EXPOSE $EXPOSED_PORT/tcp

