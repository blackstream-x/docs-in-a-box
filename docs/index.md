# Docs in a Box

A project to build documentation with [MkDocs](https://www.mkdocs.org/)
and provide it in a docker container
on top of [nginx](https://nginx.org/).


