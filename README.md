# Docs in a Box

Example project generating a static HTML site
using [MkDocs](https://www.mkdocs.org/),
providing it as a docker container built on top of [nginx](https://nginx.org/).

The built container images are available from registry.gitlab.com,
see <https://gitlab.com/blackstream-x/docs-in-a-box/container_registry>
for the exact coordinates.

